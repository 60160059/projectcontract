/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contractprogram;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class UserTableModel extends AbstractTableModel{
    String[] columnNames = {"id", "Username", "name", "surname"};
    ArrayList<User> userList = Data.userList;
    public UserTableModel(){
        
    }
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userList.get(rowIndex);
        switch(columnIndex){
            case 0: return user.getId();
            case 1: return user.getLogin();
            case 2: return user.getName();
            case 3: return user.getSurname();
        }
        return "";
    }
    
}
